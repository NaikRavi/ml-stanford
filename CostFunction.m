function J = CostFunction(X, y, theta)
	m = size(X, 1);
	predictions = X * theta;
	SquaredError = (predictions - y) .^ 2;
	J = 1/(2*m)*sum(SquaredError);