function plotJ(X, y)
%PLOTDATA Plots the data points x and y into a new figure 
%   PLOTDATA(x,y) plots the data points and gives the figure axes labels of
%   population and profit.

 % open a new figure window

% ====================== YOUR CODE HERE ======================
% Instructions: Plot the training data into a figure using the 
%               "figure" and "plot" commands. Set the axes labels using
%               the "xlabel" and "ylabel" commands. Assume the 
%               population and revenue data have been passed in
%               as the x and y arguments of this function.
%
% Hint: You can use the 'rx' option with plot to have the markers
%       appear as red crosses. Furthermore, you can make the
%       markers larger by using plot(..., 'rx', 'MarkerSize', 10);
theta0 = linspace(-10, 10, 100);
theta1 = linspace(-1, 4, 100);

J_vals = zeros(length(theta0), length(theta1));
for i=1:length(theta0)
	for j=1:length(theta1)
		t = [theta0(i);theta1(j)];
		J_vals(i,j) = computeCost(X, y, t);
	end
end
J_vals = J_vals';		
figure;
surf(theta0, theta1, J_vals);
xlabel('Theta0');ylabel('Theta1');

figure;
contour(theta0, theta1, J_vals, logspace(-2, 3, 20));
xlabel('Theta0');ylabel('Theta1');

% ============================================================

end
